<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSS de Bootstrap -->
	<link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
	    <script src="../js/bootstrap.min.js"></script>
	<!-- CSS -->
	<link href="../../css/css/reset.css" rel="stylesheet">

	<title>Inicio</title>
</head>
<div class="container container nopadding menu col-md-3">
	<?php
		include '../post/resumeSlide.php';
		include '../createPost/toolsResume.php';
	?>
</div>
<div class="container container nopadding menu col-md-8">
	<?php

		//include 'post/introPost.php';
		include 'stepPost.php';
		//include 'toolsPost.php';
		//include 'post/materialPost.php';

	?>
	<input type="button" name="" value="Siguiente paso" class="pull-right">

	<?php
		include '../all/commentary.php';
		include '../all/createCommentary.php';
	?>
</div>