<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <!-- jQuery -->
	    <script src="../../js/jquery.js"></script>
	    <!-- Bootstrap Core JavaScript -->
	    <script src="../../js/bootstrap.min.js"></script>

		<!-- CSS de Bootstrap -->
		<link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		    <script src="../../js/bootstrap.min.js"></script>
		<!-- CSS -->
		<link href="../../css/css/reset.css" rel="stylesheet">
		<link href="../../css/css/colors.css" rel="stylesheet">
		<link href="../../css/css/css.css" rel="stylesheet">
	    
		<title>Inicio</title>
	</head>
	<body>
		<?php 
			include '../all/head.php';
			include '../all/menu.php';
		?>
		<div class="container col-md-10 wall">
			<?php
				include '../all/postResume.php';
				//include '../all/userResume.php';
			?>
		</div>
	</body>

</html>