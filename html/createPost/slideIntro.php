<div class="container  col-md-11 slideEdition">
	<div class="form-group">
    	<div class="col-lg-3 text noppadign">
	    	<label class="control-label text col-md-12 ">Titulo de post</label>
	    </div>
	    <div class="col-lg-9 input">
	      <input type="text" class="form-control input" id="name"
	             placeholder="Titulo" required="true">
	    </div>
    </div>
    <div class="form-group">
    	<div class="col-lg-3 text noppadign">
	    	<label class="control-label text col-md-12 ">Descripción</label>
	    </div>
	    <div class="col-lg-9 input">
	      <textarea class="form-control input" id="name"
	             placeholder="Descripción" required="true"></textarea>
	    </div>
    </div>
    <div class="form-group">
    	<div class="col-lg-3 text noppadign">
	    	<label class="control-label text col-md-12 ">Imagen de portada</label>
	    </div>
	    <div class="col-lg-9 input">
	      <input type="file" name="">
	    </div>
    </div>
    <div class="form-group col-md-12">
    	<!-- Categoria -->
    	<div class="col-md-2 input  pull-right">
	      	<select class=" col-md-12 ">
				<option>Categoria</option>
			</select>
	    </div>
    	<div class="col-md-1 text noppadign pull-right">
	    	<label class="control-label text col-md-12 ">Categoria</label>
	    </div>
	    

	    <!-- Tipo -->
	    <div class="col-md-2 input  pull-right">
	      	<select class="col-md-12">
				<option>Tipo</option>
			</select>
	    </div>
	    <div class="col-md-1 text noppadign pull-right">
	    	<label class="control-label text col-md-12 ">Tipo</label>
	    </div>
	    
		<!-- Lugar -->
	    <div class="col-md-2 input  pull-right">
	      	<select class="col-md-12">
				<option>Lugar</option>
			</select>
	    </div>
	    <div class="col-md-1 text noppadign pull-right">
	    	<label class="control-label text col-md-12 ">Lugar</label>
	    </div>
	    

    </div>
	<input type="button" name="" value="Siguiente paso" class="pull-right nextButton primaryColor text-whiteColor">
</div>