<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <!-- jQuery -->
	    <script src="../../js/jquery.js"></script>
	    <!-- Bootstrap Core JavaScript -->
	    <script src="../../js/bootstrap.min.js"></script>

		<!-- CSS de Bootstrap -->
		<link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
		    <script src="../../js/bootstrap.min.js"></script>
		<!-- CSS -->
		<link href="../../css/css/reset.css" rel="stylesheet">
		<link href="../../css/css/colors.css" rel="stylesheet">
		<link href="../../css/css/css.css" rel="stylesheet">
	    
	<title>Crear post</title>
</head>
<body>
	<?php
		include '../all/head.php';
	?>
	<div class="container container nopadding menu col-md-2 whiteColor">
		<?php
			include 'slideResume.php';
			//include 'toolsResume.php';
		?>
	</div>
	<form>
	<div class="container wall col-md-10">
		<?php
			include 'slideIntro.php';
			//include 'slideStep.php';
			//include 'slideTools.php';
			//include 'slideMaterial.php';
		?>
		<div class="col-md-1 pull-right">
			<input type="submit" name="" value="Publicar" class="pull-right adviseColor nextButton text-whiteColor">			
		</div>
		<div class="col-md-1 pull-right">
			<input type="button" name="" value="Guardar" class="pull-right">			
		</div>

	</div>
	</form>
</body>
</html>