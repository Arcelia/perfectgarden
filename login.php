<!DOCTYPE html>
<html>
<head>
 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Garden </title>

	
	<!-- CSS -->
	<link href="css/login.css" rel="stylesheet">
	<link href="css/css/colors.css" rel="stylesheet">
	<link href="css/css/reset.css" rel="stylesheet">
	<!-- CSS de Bootstrap -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<script src="js/jquery-3.2.0.min.js"></script>
	    <script src="js/bootstrap.min.js"></script>
</head>
	<!--- Block -->
	<div class="block col-md-6 primaryColor" ></div>

	<!-- Registro -->
	<div class="container col-md-6 registerLogin backgroundRegister">
		<div class="col-md-8  col-md-offset-2 login">
		<form>
			<h1>Registro</h1>
				<div class="form-group">
			    	<div class="col-lg-3 text noppadign">
				    	<label class="control-label text">Nombre</label>
				    </div>
				    <div class="col-lg-9 input">
				      <input type="text" class="form-control input" id="name"
				             placeholder="Nombre de usuario" required="true">
				    </div>
			    </div>
			    <div class="form-group">
			    	<div class="col-lg-3 text noppadign">
				    	<label class="control-label text">Correo</label>
				    </div>
				    <div class="col-lg-9 input">
				      <input type="email" class="form-control input" id="ejemplo_email_3"
				             placeholder="Correo" required="true">
				    </div>
			    </div>
			    <div class="form-group">
			    	<div class="col-lg-3 text noppadign">
				    	<label class="control-label text">Contraseña</label>
				    </div>
				    <div class="col-lg-9 input">
				      <input type="password" class="form-control input" id="ejemplo_email_3"
				             placeholder="Contraseña" required="true">
				    </div>
			    </div>
			    <div class="form-group">
			    	<div class="col-lg-3 text noppadign">
				    	<label class="control-label text">Fecha de nacimiento</label>
				    </div>
				    <div class="col-lg-9 input">
				      <input type="date" class="form-control input" id="ejemplo_email_3">
				    </div>
			    </div>
			    <div class="radio-inline">
				</div>
			    <div class="radio-inline">
				  <label>
				    <input type="radio" name="gender" value="m" checked>
				    Mujer
				  </label>
				</div>
				<div class="radio-inline">
				  <label>
				    <input type="radio" name="gender" value="h">
				    Hombre
				  </label>
				</div>
			    <div class="col-md-6 noppadign" >
			    	<input type="button" class="btn pull-right text col-lg-12 primaryColor-black" id="right" value="Ya tengo una cuenta">
			    </div>
			    <div class="col-md-6">
			    	<input type="submit" class="btn pull-right text col-lg-12 primaryColor" value="Crear cuenta">
			    </div>
			</form>
		</div>

	</div>

	<!-- Login-->
	<div class="container col-md-6 registerLogin backgroundLogin">
		<div class=" col-md-8  col-md-offset-3 login">
			<form action="" method="post" id="login">
				<h1>Inicia sesion</h1>
				<div class="form-group">
					<div class="col-lg-3 text noppadign">
				    	<label class="control-label text">Correo</label>
				    </div>
				    <div class="col-lg-9 input">
				      <input type="email" class="form-control input" id="ejemplo_email_3"
				             placeholder="Correo" required="true">
				    </div>

				   
				   <div class="col-lg-3 text noppadign">
				    	<label class="control-label text">Contraseña</label>
				    </div>
				    <div class="col-lg-9 input">
				      <input type="password" class="form-control input" id="ejemplo_email_3"
				             placeholder="Contraseña" required="true">
				    </div>
				    	
				    <div class="col-md-1">
				    	<input type="checkbox" name="remember" >
				    </div>
				    <div class="col-lg-10">
				    	<p class="control-label text-whiteColor">Recordar contraseña</p>
				    	<p class="control-label text-adviseColor-light">¿Olvidaste tu contraseña?</p>
				    </div>
				    
				    <div class="col-md-6">
			    		<input type="button" class="btn pull-right text col-md-12 primaryColor-black" id="left" value="Crear cuenta">
			    	</div>
				    <div class="col-md-6">
			    		<input type="submit" class="btn pull-right text col-md-12 primaryColor" value="Entrar">
			    	</div>
			    </div>
			</form>
		</div>
	</div>

	<script src="js/js/js.js"></script>
</body>
</html>